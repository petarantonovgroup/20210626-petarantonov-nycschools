//
//  NSString+Email.h
//  20210626-PetarAntonov-NYCSchools
//
//  Created by Petar Antonov on 6/27/21.
//

#import <Foundation/Foundation.h>

@interface NSString (Email)

- (BOOL)isValidEmail;

@end
