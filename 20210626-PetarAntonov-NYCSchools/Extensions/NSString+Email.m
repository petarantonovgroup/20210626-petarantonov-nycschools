//
//  NSString+Email.m
//  20210626-PetarAntonov-NYCSchools
//
//  Created by Petar Antonov on 6/27/21.
//

#import "NSString+Email.h"

// This class will provide additional methods connected with an email address
@implementation NSString (Email)

- (BOOL)isValidEmail
{
    NSString *emailRegex = @"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES[c] %@", emailRegex];
    return [emailTest evaluateWithObject:self];
}

@end
