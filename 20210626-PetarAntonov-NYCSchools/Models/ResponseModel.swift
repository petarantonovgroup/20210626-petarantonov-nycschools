//
//  ResponseModel.swift
//  20210626-PetarAntonov-NYCSchools
//
//  Created by Petar Antonov on 6/26/21.
//

import UIKit

enum ResponseCode: Int {
    case CodeUnknown = 0    // Unknown
    case Code200     = 200  // Success
    case Code204     = 204  // Success with empty response
    case Code1100    = 1100 // Server response wrong format
}

// This class will be used to transfer converted server data from the request to the requester
class ResponseModel: NSObject {
    
    public var code: ResponseCode?
    public var data: [String: Any]?
    
    override init() {
        self.code = .CodeUnknown
    }
    
    init(code: ResponseCode){
        self.code = code
    }
    
    public func getResponseCode() -> ResponseCode {
        return self.code ?? .CodeUnknown
    }
    
}
