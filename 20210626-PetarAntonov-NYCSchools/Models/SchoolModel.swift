//
//  SchoolModel.swift
//  20210626-PetarAntonov-NYCSchools
//
//  Created by Petar Antonov on 6/26/21.
//

import UIKit
import CoreLocation

@objc class SchoolModel: NSObject {
    
    public var id: String?
    @objc public var name: String?
    @objc public var phone: String?
    @objc public var fax: String?
    @objc public var email: String?
    @objc public var website: String?
    // TODO: It would be better to encapsulate the address fields in a separate class
    @objc public var address: String?
    @objc public var city: String?
    @objc public var zip: String?
    @objc public var state: String?
    @objc public var latitude: CLLocationDegrees = 0
    @objc public var longitude: CLLocationDegrees = 0
    
    @objc public var satMathScore: String?
    @objc public var satReadingScore: String?
    @objc public var satWritingScore: String?
    
    // Copy the SAT fields
    public func addSatScores(school: SchoolModel) {
        // Don't assign the SAT data if the schools have different ids
        if self.id != school.id {
            return
        }
        self.satMathScore = school.satMathScore
        self.satReadingScore = school.satReadingScore
        self.satWritingScore = school.satWritingScore
    }
    
    // Format the school address baased on the available fields
    @objc public func formattedAddess() -> String {
        var fullAddress: String = ""
        
        if let address = self.address {
            fullAddress = "\(address)\n"
        }
        if let city = self.city {
            fullAddress.append("\(city), ")
        }
        if let state = self.state {
            fullAddress.append("\(state), ")
        }
        if let zip = self.zip {
            fullAddress.append("\(zip)")
        }
        
        // If the school have city or/and state and missing zip, there would be ", " at the end that has to be removed
        if fullAddress.hasSuffix(", ") {
            fullAddress.removeLast(2)
        }
        
        return fullAddress
    }
}
