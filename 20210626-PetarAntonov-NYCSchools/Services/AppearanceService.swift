//
//  AppearanceService.swift
//  20210626-PetarAntonov-NYCSchools
//
//  Created by Petar Antonov on 6/26/21.
//

import UIKit

class AppearanceService: NSObject {
    
    // Setup different global appearances
    static public func setupAppearance() {
        AppearanceService.setupNavigationBar()
    }
    
    // Setup the navigation bar appearance
    private static func setupNavigationBar() {
        // Make the navigation bar non translucent (solid)
        UINavigationBar.appearance().isTranslucent = false
        
        // Rremove the little line at the bottom of the navigation bar
        UINavigationBar.appearance().shadowImage = UIImage()
        
        // Set the tint(background) color for the navigation bar
        UINavigationBar.appearance().barTintColor = UIColor(named: "mainColor")
        
        // Set the text color for the navigation bar buttons
        UIBarButtonItem.appearance().tintColor = UIColor(named: "secondaryMainColor")
        
        // Set the text color and font size for the navigation bar title
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor(named: "secondaryMainColor")!,
                                                            NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .headline)]
    }
    
}
