//
//  LoadingService.swift
//  20210626-PetarAntonov-NYCSchools
//
//  Created by Petar Antonov on 6/26/21.
//

import UIKit

@objc class LoadingService: NSObject {
    
    var loadingView: LoadingView?
    var isShown: Bool = false
    
    // Create singleton
    public static var shared: LoadingService = {
        let loadingService = LoadingService()
        
        loadingService.loadingView = LoadingView.init(frame: UIScreen.main.bounds)
        
        return loadingService
    }()
    
    // The shared method can be reached from ObjC
    @objc class func sharedInstance() -> LoadingService {
        return LoadingService.shared
    }
    
    private override init() {}
    
    // MARK: Other methods
    
    @objc public func showLoadingView() {
        if self.isShown {
            return;
        }
        
        guard let sceneDelegate: SceneDelegate = UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate else { return }
        
        // Disable the "swipe to close" gesture
        if sceneDelegate.window?.rootViewController is UINavigationController {
            let rootViewController: UINavigationController = sceneDelegate.window?.rootViewController as! UINavigationController
            rootViewController.interactivePopGestureRecognizer?.isEnabled = false
        }
        
        // Create thes loading view and place it on top of the screen
        self.loadingView?.frame = UIScreen.main.bounds
        sceneDelegate.window?.addSubview(self.loadingView!)
        
        self.isShown = true
    }
    
    @objc public func hideLoadingView() {
        guard let sceneDelegate: SceneDelegate = UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate else { return }
        
        // Enable the swipe to close gesture
        if sceneDelegate.window?.rootViewController is UINavigationController {
            let rootViewController: UINavigationController = sceneDelegate.window?.rootViewController as! UINavigationController
            rootViewController.interactivePopGestureRecognizer?.isEnabled = true
        }
        
        // Remove the loading view safely
        if Thread.isMainThread {
            self.loadingView?.removeFromSuperview()
        } else {
            DispatchQueue.main.async {
                self.loadingView?.removeFromSuperview()
            }
        }
        
        self.isShown = false
    }
    
}
