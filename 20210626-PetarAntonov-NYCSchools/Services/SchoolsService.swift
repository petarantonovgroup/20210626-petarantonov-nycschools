//
//  SchoolsService.swift
//  20210626-PetarAntonov-NYCSchools
//
//  Created by Petar Antonov on 6/26/21.
//

import UIKit

// This class will hold the schools data once it's beign loaded from the server
class SchoolsService: NSObject {
    
    public var schools: [SchoolModel] = [SchoolModel]()
    
    // Create singleton
    public static var shared: SchoolsService = {
        let schoolsService = SchoolsService()
        
        return schoolsService
    }()
    
    // The shared method can be reached from ObjC
    @objc class func sharedInstance() -> SchoolsService {
        return SchoolsService.shared
    }
    
    private override init() {}
    
}
