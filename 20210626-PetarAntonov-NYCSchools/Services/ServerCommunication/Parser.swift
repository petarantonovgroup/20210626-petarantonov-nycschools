//
//  Parser.swift
//  20210626-PetarAntonov-NYCSchools
//
//  Created by Petar Antonov on 6/26/21.
//

import UIKit

class Parser: NSObject {
    
    // Safly unwrap a server value or throw an exception
    class func unwrap<serverValue>(_ optional: serverValue?) throws -> serverValue {
        if let real = optional {
            return real
        } else {
            throw NSError(domain: "", code: 0, userInfo: [String: String]())
        }
    }
    
    // Process the server schools data
    // The expected result is an array of dictionaries
    // If something else is returned, ignore the result
    @objc class func parserForGetSchools(responseObject: [Any]) -> ResponseModel {
        
        let response: ResponseModel = ResponseModel()
        
        if let resultArray: [[String: Any]] = responseObject as? [[String: Any]] {
            do {
                var schools: [SchoolModel] = [SchoolModel]()
                
                for serverSchool: [String: Any] in resultArray {
                    let school: SchoolModel = SchoolModel()
                    
                    // Assuming the following fields are required
                    school.id = try Parser.unwrap(serverSchool["dbn"] as? String)
                    school.name = try Parser.unwrap(serverSchool["school_name"] as? String)
                    
                    // Assuming the following fields are optional
                    if serverSchool["phone_number"] != nil {
                        school.phone = try Parser.unwrap(serverSchool["phone_number"] as? String)
                    }
                    if serverSchool["fax_number"] != nil {
                        school.fax = try Parser.unwrap(serverSchool["fax_number"] as? String)
                    }
                    if serverSchool["school_email"] != nil {
                        school.email = try Parser.unwrap(serverSchool["school_email"] as? String)
                    }
                    if serverSchool["website"] != nil {
                        school.website = try Parser.unwrap(serverSchool["website"] as? String)
                    }
                    if serverSchool["primary_address_line_1"] != nil {
                        school.address = try Parser.unwrap(serverSchool["primary_address_line_1"] as? String)
                    }
                    if serverSchool["city"] != nil {
                        school.city = try Parser.unwrap(serverSchool["city"] as? String)
                    }
                    if serverSchool["zip"] != nil {
                        school.zip = try Parser.unwrap(serverSchool["zip"] as? String)
                    }
                    if serverSchool["state_code"] != nil {
                        school.state = try Parser.unwrap(serverSchool["state_code"] as? String)
                    }
                    if serverSchool["latitude"] != nil && serverSchool["longitude"] != nil {
                        school.latitude = Double(try Parser.unwrap(serverSchool["latitude"] as? String)) ?? 0
                        school.longitude = Double(try Parser.unwrap(serverSchool["longitude"] as? String)) ?? 0
                    }
                    
                    schools.append(school)
                }
                
                response.data = ["schools": schools]
                response.code = .Code200
                
            } catch {
                response.code = .Code1100
            }
        } else {
            response.code = .Code1100
        }
        
        return response
    }
    
    // The expected result is an array with one element(dictinary) if the school have SAT data
    // If something else is returned, ignore the result
    @objc class func parserForGetSchoolSat(responseObject: [Any]) -> ResponseModel {
        
        let response: ResponseModel = ResponseModel()
        
        if let resultArray: [[String: Any]] = responseObject as? [[String: Any]] {
            do {
                let school: SchoolModel = SchoolModel()
                
                // Don't handle the result if received something else than array with one element
                if resultArray.count == 1 {
                    let serverSchool: [String: Any] = resultArray[0]
                    
                    // Assuming the following fields are required
                    school.id = try Parser.unwrap(serverSchool["dbn"] as? String)
                    school.satMathScore = try Parser.unwrap(serverSchool["sat_math_avg_score"] as? String)
                    school.satReadingScore = try Parser.unwrap(serverSchool["sat_critical_reading_avg_score"] as? String)
                    school.satWritingScore = try Parser.unwrap(serverSchool["sat_writing_avg_score"] as? String)
                }
                response.data = ["school": school]
                response.code = .Code200
            } catch {
                response.code = .Code1100
            }
        } else {
            response.code = .Code1100
        }
        
        return response
    }
    
}
