//
//  ServerCommunication.swift
//  20210626-PetarAntonov-NYCSchools
//
//  Created by Petar Antonov on 6/26/21.
//

import UIKit
import Alamofire

class ServerCommunication: NSObject {
    
    // Server endpoint and API key
    static let ServerCommunicationEndPoint: String = "https://data.cityofnewyork.us"
    static let APIKey: String = "jklEp0kMwxm26QEGmKTiy8bJa"
    
    // Create singleton
    public static var shared: ServerCommunication = {
        let serverCommunication = ServerCommunication()
        
        return serverCommunication
    }()
    
    // The shared method can be reached from ObjC
    @objc class func sharedInstance() -> ServerCommunication {
        return ServerCommunication.shared
    }
    
    private override init() {}
    
    // MARK: Alamofire
    
    private func startRequest(path: String, method: String, params: [String: Any], parser: Selector, completion: ((ResponseModel) -> Void)?) {
        // Creathe the URL and request
        let url: URL = URL(string: "\(ServerCommunication.ServerCommunicationEndPoint)\(path)")!
        var urlRequest: URLRequest = URLRequest(url: url)
        
        // Set the request HTTP method
        urlRequest.httpMethod = method
        
        print("Request start: \(method) \(path)")
        
        // Set request body
        // Params can't be empty. Return server error otherwise
        if !params.isEmpty {
            do {
                urlRequest.httpBody = try JSONSerialization.data(withJSONObject: params, options: [])
            } catch {
                print("Error with JSON serialization")
                let responseModel: ResponseModel = ResponseModel(code: .Code1100)
                if completion != nil {
                    completion!(responseModel)
                }
            }
        }
        
        // Set request headers
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.setValue(ServerCommunication.APIKey, forHTTPHeaderField: "X-App-Token")
        
        // Set standart timeout (in seconds)
        AF.sessionConfiguration.timeoutIntervalForRequest = 30
        // Wait for internet connectivity
        AF.sessionConfiguration.waitsForConnectivity = true
        
        // Start the request
        AF.request(urlRequest).responseJSON { response in
            
            switch response.result {
            case .success(let resultData):
                // Handle the request success
                print("Request finish success: \(method) \(path)")
                
                // Create response model and assign a status code and data if applicable
                var responseModel: ResponseModel = ResponseModel()
                if response.response?.statusCode == 204 {
                    responseModel.code = .Code204
                } else if response.response?.statusCode == 200  {
                    responseModel = (Parser.perform(parser, with: resultData)?.takeUnretainedValue())! as! ResponseModel
                } else {
                    // Unexpected success code
                    responseModel.code = (response.response?.statusCode).map { ResponseCode(rawValue: $0) } ?? .Code1100
                }
                
                // Return result
                if completion != nil {
                    completion!(responseModel)
                }
            case .failure(let error):
                // Handle the request failure
                print("Request finish failure: \(method) \(path)")
                print("Error: \(error)")
                
                // Create response model and assign a status code if possible
                let responseModel: ResponseModel = ResponseModel()
                responseModel.code = (response.response?.statusCode).map { ResponseCode(rawValue: $0) } ?? .Code1100
                
                if completion != nil {
                    completion!(responseModel)
                    
                }
            }
        }
    }
    
    // MARK: Other methods
    
    // MARK: Schools
    
    // Get the school data sorted by the school name
    public func getSchools(completion: ((ResponseModel) -> Void)?) {
        let params: [String: Any] = [String: Any]()
        
        startRequest(path: "/resource/s3k6-pzi2.json?$order=school_name", method: "GET", params: params, parser: #selector(Parser.parserForGetSchools(responseObject:)), completion: completion)
    }
    
    // Get a particular school SAT data
    public func getSchoolSAT(schoolId: String, completion: ((ResponseModel) -> Void)?) {
        let params: [String: Any] = [String: Any]()
        
        startRequest(path: "/resource/f9bf-2cp4.json?dbn=\(schoolId)", method: "GET", params: params, parser: #selector(Parser.parserForGetSchoolSat(responseObject:)), completion: completion)
    }
    
}
