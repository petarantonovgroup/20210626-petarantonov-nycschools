//
//  UtilityService.h
//  20210626-PetarAntonov-NYCSchools
//
//  Created by Petar Antonov on 6/27/21.
//

#import <Foundation/Foundation.h>

@interface UtilityService : NSObject

+ (NSAttributedString *)convertToLink:(NSString *)string;

@end
