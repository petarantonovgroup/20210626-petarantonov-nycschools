//
//  UtilityService.m
//  20210626-PetarAntonov-NYCSchools
//
//  Created by Petar Antonov on 6/27/21.
//

#import "UtilityService.h"
#import <UIKit/UIKit.h>

// This class will provide various utility methods
@implementation UtilityService

+ (NSAttributedString *)convertToLink:(NSString *)string
{
    // Style a string to look like a clickable link
    NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor colorNamed:@"mainColor"],
                                 NSUnderlineColorAttributeName: [UIColor colorNamed:@"mainColor"],
                                 NSUnderlineStyleAttributeName: @YES};
    return [[NSAttributedString alloc] initWithString:string attributes:attributes];
}

@end
