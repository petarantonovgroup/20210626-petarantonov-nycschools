//
//  BaseObjCViewController.h
//  20210626-PetarAntonov-NYCSchools
//
//  Created by Petar Antonov on 6/27/21.
//

#import <UIKit/UIKit.h>

@interface BaseObjCViewController : UIViewController

@end
