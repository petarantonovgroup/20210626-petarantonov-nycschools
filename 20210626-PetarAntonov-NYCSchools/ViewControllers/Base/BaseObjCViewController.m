//
//  BaseObjCViewController.m
//  20210626-PetarAntonov-NYCSchools
//
//  Created by Petar Antonov on 6/27/21.
//

#import "BaseObjCViewController.h"

// Base view controller class
// Every Objective C view controller should inherit it
// This class is needed because Objective C can't inherit from Swift, so we can't use BaseViewController.
// This class should match everything from BaseViewController
@interface BaseObjCViewController ()

@end

@implementation BaseObjCViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Remove the "Back" from the next screen back button
    self.navigationItem.backButtonTitle = @" ";
    
    // Set the screen background color
    self.view.backgroundColor = [UIColor colorNamed:@"screenBackgroundColor"];
}

@end
