//
//  BaseViewController.swift
//  20210626-PetarAntonov-NYCSchools
//
//  Created by Petar Antonov on 6/26/21.
//

import UIKit

// Base view controller class
// Every swift view controller should inherit it
// This class should match everything from BaseObjCViewController
class BaseViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Remove the "Back" from the next screen back button
        self.navigationItem.backButtonTitle = " "
        
        // Set the screen background color
        self.view.backgroundColor = UIColor(named: "screenBackgroundColor")
    }
    
}
