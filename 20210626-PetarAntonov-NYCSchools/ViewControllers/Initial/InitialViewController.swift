//
//  InitialViewController.swift
//  20210626-PetarAntonov-NYCSchools
//
//  Created by Petar Antonov on 6/26/21.
//

import UIKit

// Use this controller as a buffer between the splash screen and the actual first screen
// Possible usages:
// - check for app updates
// - check for minimum supported app version
// - load server data
// When everything is completed you can navigate to the home screen
class InitialViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Start loading the schools data
        getSchools(showLoading: false)
    }
    
    private func getSchools(showLoading: Bool) {
        if showLoading {
            LoadingService.shared.showLoadingView()
        }
        
        ServerCommunication.shared.getSchools { [weak self] response in
            if showLoading {
                LoadingService.shared.hideLoadingView()
            }
            
            // Handle the server response
            if response.code == .Code200 {
                guard let schools: [SchoolModel] = response.data?["schools"] as? [SchoolModel] else {
                    self?.showSchoolsError()
                    return
                }
                
                // Assign the school data to the school service and open the home screen
                SchoolsService.shared.schools = schools
                let sceneDelegate: SceneDelegate = self?.view.window?.windowScene?.delegate as! SceneDelegate
                sceneDelegate.navigateToHome(animated: false)
            } else {
                self?.showSchoolsError()
            }
        }
    }
    
    private func showSchoolsError() {
        let alert = UIAlertController(title: "Information", message: "Unable to retrive the schools data. Please try again.", preferredStyle: .alert)
        let retryAction = UIAlertAction(title: "Retry", style: .cancel, handler: { [weak self] (action) -> Void in
            self?.getSchools(showLoading: true)
        })
        alert.addAction(retryAction)
        present(alert, animated: true, completion: nil)
    }

}
