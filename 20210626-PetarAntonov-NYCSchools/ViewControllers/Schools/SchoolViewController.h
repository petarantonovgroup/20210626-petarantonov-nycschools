//
//  SchoolViewController.h
//  20210626-PetarAntonov-NYCSchools
//
//  Created by Petar Antonov on 6/27/21.
//

#import "BaseObjCViewController.h"

@class SchoolModel;

@interface SchoolViewController : BaseObjCViewController

@property (nonatomic, strong) SchoolModel *school;

@end
