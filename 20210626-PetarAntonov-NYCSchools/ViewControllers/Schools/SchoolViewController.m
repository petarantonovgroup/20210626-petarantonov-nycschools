//
//  SchoolViewController.m
//  20210626-PetarAntonov-NYCSchools
//
//  Created by Petar Antonov on 6/27/21.
//

#import "SchoolViewController.h"
#import "PetarAntonovNYCSchools-Swift.h"
#import "UtilityService.h"
#import "NSString+Email.h"
#import <MessageUI/MessageUI.h>
#import <MapKit/MapKit.h>

@import SafariServices;

// This controller is used to present a school details
// It features:
// - showing school data inside of a scroll view
// - if corectly formatted and possible, "tap to call" the school phone number
// - if corectly formatted and possible, "tap to email" the school email address
// - if corectly formatted, "tap to open" the school website
// - if available, "tap to call" the school location inside the iOS maps application
@interface SchoolViewController () <MFMailComposeViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIView *wrapperView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *faxLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UILabel *websiteLabel;
@property (weak, nonatomic) IBOutlet UILabel *satMathLabel;
@property (weak, nonatomic) IBOutlet UILabel *satReadingLabel;
@property (weak, nonatomic) IBOutlet UILabel *satWritingLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet MKMapView *addressMapView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *addressMapViewHeight;

@end

@implementation SchoolViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = @"School details";
    
    // Add "Close" button to the navigation bar to close the presented screen
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Close" style:UIBarButtonItemStylePlain target:self action:@selector(closeButtonTapped:)];
    
    [self updateMainUI];
}

#pragma mark - Actions

- (void)closeButtonTapped:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)phoneNumberLabelTapped:(id)sender
{
    // Opens the system phone call prompt alert
    NSString *phoneNumber = [@"telprompt://" stringByAppendingString:[self.school.phone stringByReplacingOccurrencesOfString:@" " withString:@""]];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber] options:@{} completionHandler:nil];
}

- (void)emailLabelTapped:(id)sender
{
    // Opens the system email composer controller
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;
    // Set the school email as recipient
    [picker setToRecipients:@[self.school.email]];
    [self presentViewController:picker animated:YES completion:nil];
}

- (void)webSiteLabelTapped:(id)sender
{
    // Open the safari controller inside the app with the school website
    NSURL *url = [NSURL URLWithString:self.school.website];
    SFSafariViewController *safariViewController = [[SFSafariViewController alloc] initWithURL:url];
    // Style the safari controller
    safariViewController.preferredControlTintColor = [UIColor colorNamed:@"mainColor"];
    
    // Encapsulate the safari controller inside of a navigation controlloer
    // So when presented it pops up like a modal controller from the bottom of the screen
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:safariViewController];
    navController.navigationBarHidden = YES;
    
    [self presentViewController:navController animated:YES completion:nil];
}

#pragma mark - MFMailComposeViewControllerDelegate

// Close the email composer controller when done
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - MKMapViewDelegate

// Open the iOS map application upon the user tapping on the annotation
- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:CLLocationCoordinate2DMake(self.school.latitude, self.school.longitude) addressDictionary:nil];
    MKMapItem *destination = [[MKMapItem alloc] initWithPlacemark:placemark];
    destination.name = self.school.name;
    NSDictionary *launchOptions = @{MKLaunchOptionsDirectionsModeKey:MKLaunchOptionsDirectionsModeDriving};
    [destination openInMapsWithLaunchOptions:launchOptions];
}

#pragma mark - Other methods

- (void)updateMainUI
{
    // Style the wrapper view
    self.wrapperView.layer.cornerRadius = 10.0;
    self.wrapperView.layer.borderWidth = 1.0;
    self.wrapperView.layer.borderColor = [UIColor colorNamed:@"mainColor"].CGColor;
    
    // Set the screen values
    if (self.school.name.length > 0) {
        self.nameLabel.text = self.school.name;
    } else {
        self.nameLabel.text = @"-";
    }
    
    [self setupPhoneLabel];
    
    if (self.school.fax.length > 0) {
        self.faxLabel.text = self.school.fax;
    } else {
        self.faxLabel.text = @"-";
    }
    
    [self setupEmailLabel];
    
    [self setupWebsiteLabel];
    
    if (self.school.satMathScore.length > 0) {
        self.satMathLabel.text = self.school.satMathScore;
    } else {
        self.satMathLabel.text = @"-";
    }
    
    if (self.school.satReadingScore.length > 0) {
        self.satReadingLabel.text = self.school.satReadingScore;
    } else {
        self.satReadingLabel.text = @"-";
    }
    
    if (self.school.satWritingScore.length > 0) {
        self.satWritingLabel.text = self.school.satWritingScore;
    } else {
        self.satWritingLabel.text = @"-";
    }
    
    NSString *formattedAddess = [self.school formattedAddess];
    if (formattedAddess.length > 0) {
        self.addressLabel.text = formattedAddess;
    } else {
        self.addressLabel.text = @"-";
    }
    
    if (self.school.latitude != 0 && self.school.longitude != 0) {
        [self setupAddressMap];
    } else {
        // Hide the map if the school location isn't available
        self.addressMapViewHeight.constant = 0.0;
        [self.view layoutIfNeeded];
    }
}

- (void)setupPhoneLabel
{
    if (self.school.phone.length > 0) {
        // Check if the phone is valid for iOS to trigger the telepromt alert
        NSString *phoneNumber = [@"telprompt://" stringByAppendingString:[self.school.phone stringByReplacingOccurrencesOfString:@" " withString:@""]];
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:phoneNumber]]) {
            // Style the label to show it's clickable
            self.phoneLabel.attributedText = [UtilityService convertToLink:self.school.phone];
            
            UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(phoneNumberLabelTapped:)];
            [self.phoneLabel addGestureRecognizer:gesture];
            // Enable the user interaction, otherwise the tap gesture won't trigger
            self.phoneLabel.userInteractionEnabled = YES;
        } else {
            self.phoneLabel.text = self.school.phone;
        }
    } else {
        self.phoneLabel.text = @"-";
    }
}

- (void)setupEmailLabel
{
    if (self.school.email.length > 0) {
        // Check if the email is valid and the device can send an email
        if ([self.school.email isValidEmail] && [MFMailComposeViewController canSendMail]) {
            // Style the label to show it's clickable
            self.emailLabel.attributedText = [UtilityService convertToLink:self.school.email];
            
            UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(emailLabelTapped:)];
            [self.emailLabel addGestureRecognizer:gesture];
            // Enable the user interaction, otherwise the tap gesture won't trigger
            self.emailLabel.userInteractionEnabled = YES;
        } else {
            self.emailLabel.text = self.school.email;
        }
    } else {
        self.emailLabel.text = @"-";
    }
}

- (void)setupWebsiteLabel
{
    if (self.school.website.length > 0) {
        // Check if the iOS can open the website
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:self.school.website]]) {
            // Style the label to show it's clickable
            self.websiteLabel.attributedText = [UtilityService convertToLink:self.school.website];
            
            UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(webSiteLabelTapped:)];
            [self.websiteLabel addGestureRecognizer:gesture];
            // Enable the user interaction, otherwise the tap gesture won't trigger
            self.websiteLabel.userInteractionEnabled = YES;
        } else {
            self.websiteLabel.text = self.school.website;
        }
    } else {
        self.websiteLabel.text = @"-";
    }
}

- (void)setupAddressMap
{
    // Create an annotation and place it on the map
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(self.school.latitude, self.school.longitude);
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    annotation.coordinate = coordinate;
    [self.addressMapView addAnnotation:annotation];
    
    // Set the map region
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(coordinate, 1000, 1000);
    MKCoordinateRegion adjustedRegion = [self.addressMapView regionThatFits:viewRegion];
    [self.addressMapView setRegion:adjustedRegion animated:NO];
}

@end
