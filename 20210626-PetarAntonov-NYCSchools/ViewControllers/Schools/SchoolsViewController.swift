//
//  SchoolsViewController.swift
//  20210626-PetarAntonov-NYCSchools
//
//  Created by Petar Antonov on 6/26/21.
//

import UIKit

// This controller is used to present a list of schools
class SchoolsViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    private let refreshControl: UIRefreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the title in the navigation bar
        self.navigationItem.title = "NYC Schools"
        
        // Remove the empty rows at the bottom of the table view
        self.tableView.tableFooterView = UIView()
        
        // Register the cell classes
        self.tableView.register(UINib(nibName: "SchoolsSchoolTableViewCell", bundle: nil), forCellReuseIdentifier: "SchoolsSchoolIdentifier")
        self.tableView.register(UINib(nibName: "SchoolsEmptyTableViewCell", bundle: nil), forCellReuseIdentifier: "SchoolsEmptyIdentifier")
        
        // Add a selector when pull to refresh is triggered
        self.refreshControl.addTarget(self, action: #selector(refreshSchools), for: .valueChanged)
        self.refreshControl.tintColor = UIColor(named: "secondaryMainColor")
        self.tableView.refreshControl = self.refreshControl
    }
    
    // MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Show the empty cell if no schools data is available
        if SchoolsService.shared.schools.isEmpty {
            return 1
        } else {
            return SchoolsService.shared.schools.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if SchoolsService.shared.schools.isEmpty {
            // Show the empty cell if no schools data is available
            let cell: SchoolsEmptyTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SchoolsEmptyIdentifier", for: indexPath) as! SchoolsEmptyTableViewCell
            
            // Handle the refresh button tap from the empty cell
            cell.refreshButtonTapped = { [weak self] in
                self?.refreshSchools()
            }
            
            return cell
        }
        
        // Setup a school cell and assign a school model
        let cell: SchoolsSchoolTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SchoolsSchoolIdentifier", for: indexPath) as! SchoolsSchoolTableViewCell
        
        cell.school = SchoolsService.shared.schools[indexPath.row]
        
        return cell
    }
    
    // MARK: - UITableViewDataDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if SchoolsService.shared.schools.isEmpty {
            // Don't continue if there isn't a school data
            // This case shouldn't be possible, but just in case
            return
        }
        
        let school: SchoolModel = SchoolsService.shared.schools[indexPath.row]
        
        getSatDetails(school: school)
    }
    
    // MARK: Other methods
    
    @objc private func refreshSchools() {
        LoadingService.shared.showLoadingView()
        
        ServerCommunication.shared.getSchools { [weak self] response in
            
            LoadingService.shared.hideLoadingView()
            // Stop the refresh control if this method was called from the "pull ot refresh"
            self?.refreshControl.endRefreshing()
            
            // Handle the server response
            if response.code == .Code200 {
                guard let schools: [SchoolModel] = response.data?["schools"] as? [SchoolModel] else {
                    self?.showSchoolsError()
                    return
                }
                
                // Assign the schools data and refresh the table view
                SchoolsService.shared.schools = schools
                self?.tableView.reloadData()
            } else {
                self?.showSchoolsError()
            }
        }
    }
    
    private func showSchoolsError() {
        let alert = UIAlertController(title: "Information", message: "Unable to retrive the schools data. Please try again.", preferredStyle: .alert)
        let closeAction = UIAlertAction(title: "Close", style: .cancel, handler:nil)
        alert.addAction(closeAction)
        let retryAction = UIAlertAction(title: "Retry", style: .default, handler: { [weak self] (action) -> Void in
            self?.refreshSchools()
        })
        alert.addAction(retryAction)
        present(alert, animated: true, completion: nil)
    }
    
    private func getSatDetails(school: SchoolModel) {
        LoadingService.shared.showLoadingView()
        
        // Get the SAT data for the selected school
        ServerCommunication.shared.getSchoolSAT(schoolId: school.id!) { [weak self] response in
            
            LoadingService.shared.hideLoadingView()
            
            // Handle the server response
            if response.code == .Code200 {
                guard let serverSchool: SchoolModel = response.data?["school"] as? SchoolModel else {
                    self?.showSchoolSatError()
                    return
                }
                
                // Assign the server data to the school object and present the details screen
                school.addSatScores(school: serverSchool)
                self?.openSchoolDetailsScreen(school: school)
            } else {
                self?.showSchoolSatError()
            }
        }
    }
    
    private func showSchoolSatError() {
        let alert = UIAlertController(title: "Information", message: "Unable to retrive the school data. Please try again.", preferredStyle: .alert)
        let closeAction = UIAlertAction(title: "Close", style: .cancel, handler:nil)
        alert.addAction(closeAction)
        present(alert, animated: true, completion: nil)
    }
    
    private func openSchoolDetailsScreen(school: SchoolModel) {
        // Create school details screen and assign the selected school object
        let schoolViewControlller: SchoolViewController = SchoolViewController()
        schoolViewControlller.school = school
        
        // Encapsulate the details screen in navigation controller and present it
        let navigationController: UINavigationController = UINavigationController.init(rootViewController: schoolViewControlller)
        self.present(navigationController, animated: true)
    }
    
}
