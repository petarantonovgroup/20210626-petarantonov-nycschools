//
//  LoadingView.swift
//  20210626-PetarAntonov-NYCSchools
//
//  Created by Petar Antonov on 6/26/21.
//

import UIKit

class LoadingView: UIView {
    
    var containerView: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadNib()
    }
    
    // Instante the view from the nib file
    func loadNib() {
        self.containerView = Bundle.main.loadNibNamed("LoadingView", owner: self, options: nil)?[0] as? UIView
        self.containerView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.containerView.frame = self.bounds
        
        self.addSubview(self.containerView)
    }
    
}
