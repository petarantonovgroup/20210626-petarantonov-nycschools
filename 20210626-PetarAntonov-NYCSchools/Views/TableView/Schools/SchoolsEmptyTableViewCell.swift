//
//  SchoolsEmptyTableViewCell.swift
//  20210626-PetarAntonov-NYCSchools
//
//  Created by Petar Antonov on 6/26/21.
//

import UIKit

class SchoolsEmptyTableViewCell:  BaseTableViewCell {
    
    @IBOutlet weak var emptyLabel: UILabel!
    
    // handle the user tap from the controller
    public var refreshButtonTapped : (() -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.emptyLabel.text = "No available schools at the moment. Tap to refresh the data."
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK - Actions
    
    @IBAction func emptyButtonTapped(_ sender: Any) {
        // handle the user tap from the controller
        if self.refreshButtonTapped != nil {
            self.refreshButtonTapped!()
        }
    }
    
}
