//
//  SchoolsSchoolTableViewCell.swift
//  20210626-PetarAntonov-NYCSchools
//
//  Created by Petar Antonov on 6/26/21.
//

import UIKit

class SchoolsSchoolTableViewCell: BaseTableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    // update the cell UI when the school data is set
    public var school: SchoolModel? {
        didSet {
            updateMainUi()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.updateMainUi()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    private func updateMainUi() {
        // set empty values and wait for the school data to be set
        self.nameLabel.text = "-"
        self.addressLabel.text = "-"
        
        guard let school: SchoolModel = self.school else {
            return
        }
        
        let name: String = school.name ?? ""
        self.nameLabel.text = name.count > 0 ? name : "-"
        
        let formattedAddress: String = school.formattedAddess()
        self.addressLabel.text = formattedAddress.count > 0 ? formattedAddress : "-"
    }
    
}
